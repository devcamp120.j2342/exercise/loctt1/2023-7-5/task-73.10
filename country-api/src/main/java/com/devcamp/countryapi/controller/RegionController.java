package com.devcamp.countryapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryapi.model.CCountry;
import com.devcamp.countryapi.model.CRegion;
import com.devcamp.countryapi.repository.ICountryRepository;
import com.devcamp.countryapi.repository.IRegionRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class RegionController {
    @Autowired
    IRegionRepository regionRepository;
    @Autowired
    ICountryRepository countryRepository;

    @GetMapping("/regions")
    public List<CRegion> getAllRegion() {
        return regionRepository.findAll();

    }

    @PostMapping("/countries/{id}/regions")
    public ResponseEntity<CRegion> createRegion(@PathVariable("id") long id, @RequestBody CRegion pRegion) {
        try {
            Optional<CCountry> countryData = countryRepository.findById(id);
            if (countryData.isPresent()) {
                CRegion regionData = new CRegion();
                regionData.setRegionCode(pRegion.getRegionCode());
                regionData.setRegionName(pRegion.getRegionName());
                regionData.setCountry(countryData.get());
                return new ResponseEntity<>(regionRepository.save(regionData), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/regions/{id}")
    public ResponseEntity<CRegion> deleteRegion(@PathVariable("id") long id) {
        try {
            regionRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country/{id}/regions")
    public List<CRegion> getRegionByCountryId(@PathVariable("id") long id) {
        return regionRepository.findByCountryId(id);
    }

    @GetMapping("regions/{id}")
    public CRegion getRegionById(@PathVariable("id") long id) {
        return regionRepository.findById(id).get();
    }

    @GetMapping("/region-count/{id}")
    public long countRegionByCountryId(@PathVariable("id") long id) {
        return regionRepository.countByCountryId(id);
    }

    @GetMapping("/region/check/{id}")
    public boolean checkRegionByCountryId(@PathVariable("id") long id) {
        return regionRepository.existsById(id);
    }
}
