package com.devcamp.cmenuapi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.cmenuapi.model.CDrink;
import com.devcamp.cmenuapi.repository.DrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DrinkController {
    @Autowired
    DrinkRepository drinkRepository;

    @GetMapping("/drink/all")
    public ResponseEntity<List<CDrink>> getAllDrink() {
        return new ResponseEntity<>(drinkRepository.findAll(), HttpStatus.OK);
    }

    @PostMapping("/drink/create")
    public ResponseEntity<CDrink> createDrink(@RequestBody CDrink pDrink) {
        try {
            return new ResponseEntity<>(drinkRepository.save(pDrink), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/drink/update/{id}")
    public ResponseEntity<CDrink> updateDrink(@RequestBody CDrink pDrink, @PathVariable("id") int id) {
        Optional<CDrink> cDrink = drinkRepository.findById(id);
        if (cDrink.isPresent()) {
            try {
                cDrink.get().setMaNuocUong(pDrink.getMaNuocUong());
                cDrink.get().setTenNuocUong(pDrink.getTenNuocUong());
                cDrink.get().setGia(pDrink.getGia());
                return new ResponseEntity<>(drinkRepository.save(cDrink.get()), HttpStatus.OK);
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/drink/delete/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") int id) {
        drinkRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/drink/details/{id}")
    public CDrink getDrinkById(@PathVariable("id") int id) {
        return drinkRepository.findById(id).get();
    }

}
