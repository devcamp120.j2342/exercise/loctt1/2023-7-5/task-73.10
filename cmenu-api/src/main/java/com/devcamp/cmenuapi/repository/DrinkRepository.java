package com.devcamp.cmenuapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cmenuapi.model.CDrink;

public interface DrinkRepository extends JpaRepository<CDrink, Integer> {

}
